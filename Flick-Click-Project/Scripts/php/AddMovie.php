<?php
define('AccessGranted', TRUE);
require 'DatabaseLogin.php';

$TargetDirectory = "../../FlickClickImages/";
$TargetFile = $TargetDirectory . basename($_FILES["MovieImage"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($TargetFile,PATHINFO_EXTENSION));

// Checks if this file was accessed by submit form.
if(isset($_POST["submit"])) {
    // Checks if file is an image
    $check = getimagesize($_FILES["MovieImage"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ". <br>";
        $uploadOk = 1;
    } else {
        echo "File is not an image. <br>";
        $uploadOk = 0;
    }

    // Check if file already exists
    if (file_exists($TargetFile)) {
        echo "Sorry, file already exists. <br>";
        $uploadOk = 0;
    }

    // Check file size (500KB)
    if ($_FILES["MovieImage"]["size"] > 500000) {
        echo "Sorry, your file is too large. <br>";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" ) {
        echo "Sorry, only JPG, JPEG & PNG files are allowed. <br>";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded. <br>";
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["MovieImage"]["tmp_name"], $TargetFile)) {
            echo "The file ". htmlspecialchars( basename( $_FILES["MovieImage"]["name"])). " has been uploaded. <br>";
        } else {
            echo "Sorry, there was an error uploading your file. <br>";
        }
    }
}
