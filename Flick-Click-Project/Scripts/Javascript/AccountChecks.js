﻿function LoginCheck(Email, Password) {
    // Use php script to check if Login is correct TODO FIX: make report validity work on chrome
    jQuery.ajax({
        url: '../Scripts/php/LoginCheck.php',
        method: "POST",
        data: { Email: Email, Password: Password, Access: true },
        success: function (data) {
            // If login is incorrect alert
            if (data === "0") {
                document.getElementById("LoginEmail").setCustomValidity("Incorrect email or password");
                document.getElementById("LoginEmail").reportValidity();
                // TODO FIX: after a failed login it will always display incorrect email or password. (Reset Validity)
                setTimeout(document.getElementById("LoginEmail").setCustomValidity(""), 3000);
            }
            // Else refresh
            else {
                window.location.reload();
            }
        }
    });
}

function CheckEmailAvailability(EmailValue) {
    document.getElementById("Email").setCustomValidity("");
    // If The email isn't empty
    if (EmailValue !== "") {
            // Check email using php script
            jQuery.ajax({
                url: '../Scripts/php/EmailCheck.php',
                method: "POST",
                data: {Email: EmailValue, Access: true},
                success: function (data) {
                    // if Email is available
                    if (data === "0") {
                        document.getElementById("Email").setCustomValidity("");
                        document.getElementById("Email").reportValidity()
                        // else alert Email is in use
                    } else {
                        document.getElementById("Email").setCustomValidity("Email is in use");
                        document.getElementById("Email").reportValidity()
                    }
                }
            });
        // else email is empty
    } else {
        document.getElementById("Email").setCustomValidity("");
        document.getElementById("Email").reportValidity()
    }
}

function CheckNameAvailability(NameValue) {
    document.getElementById("SignUpUsername").setCustomValidity("");

    // If name isn't empty
    if(NameValue !== ""){
        // If name fits regex
        if(/^[a-zA-Z0-9]+$/i.test(NameValue)){
            // Use php script to check name
            jQuery.ajax({
                url: '../Scripts/php/NameCheck.php',
                method: "POST",
                data: { Name: NameValue, Access: true },
                success: function (data) {
                    // If the name is available
                    if (data === "0") {
                        document.getElementById("SignUpUsername").setCustomValidity("");
                    }
                    // If the name is in use
                    else {
                        document.getElementById("SignUpUsername").setCustomValidity("Name is in use");
                    }
                }
            });
        }
        // Else name doesn't fit regex alert
        else
        {
            document.getElementById("SignUpUsername").setCustomValidity("Name is invalid");
        }
    }
    // Else name is empty hide name state
    else{
        document.getElementById("SignUpUsername").setCustomValidity("");
    }

}

function CheckPasswordMatch(input) {
    // If the Confirm password doesn't match Password
    if (input.value !== document.getElementById('CheckPassword').value) {
        input.setCustomValidity('Password must match.');
        input.reportValidity();
        // Else it matches
    } else {
        input.setCustomValidity('');
    }
}