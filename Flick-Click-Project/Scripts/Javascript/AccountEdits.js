function ChangeEmail(Name, NewEmailValue, PasswordValue) {
    // If email form is being checked
    if(document.getElementById("Email").checkValidity()){
        document.getElementById("PasswordResult").src = "../Resources/Loading.gif";
        document.getElementById("PasswordResult").style.visibility = "visible";

        // Check Password
        jQuery.ajax({
            url: '../Scripts/php/PasswordCheck.php',
            method: "POST",
            data: { Name: Name, PasswordCheck: PasswordValue, NewEmail: NewEmailValue, Access: true },
            success: function (data) {
                // If the password is wrong alert
                if (data === "0") {
                    document.getElementById("PasswordResult").src = "../Resources/NotAvailable.png";
                    document.getElementById("Password").setCustomValidity("Incorrect password");
                    document.getElementById("Password").reportValidity()
                }
                // Else change email using php script
                else
                {
                    document.getElementById("Password").setCustomValidity("");
                    document.getElementById("PasswordResult").style.visibility = "hidden";
                    jQuery.ajax({
                        url: '../Scripts/php/ChangeEmail.php',
                        method: "POST",
                        data: { Name: Name, Email: NewEmailValue, Access: true },
                        success: function () {
                            window.location.href = "Account.php";
                        }
                    });
                }
            }
        });
    }
}

function EditPassword(Name, PasswordValue, NewPasswordValue) {

        document.getElementById("ConfirmNewPassword").setCustomValidity("");
        document.getElementById("CurrentPassword").setCustomValidity("");

        // Check Password
        jQuery.ajax({
            url: '../Scripts/php/PasswordCheck.php',
            method: "POST",
            data: { Name: Name, PasswordCheck: PasswordValue, NewPassword: NewPasswordValue, Access: true },
            success: function (data) {
                // If it is wrong alert
                if (data === "0") {
                    document.getElementById("CurrentPasswordResult").src = "../Resources/NotAvailable.png";
                    document.getElementById("CurrentPassword").setCustomValidity("Incorrect password");
                    document.getElementById("CurrentPassword").reportValidity()
                }
                // Else change password with php script if new password and confirm password
                else
                {
                    if(NewPassword.value === ConfirmNewPassword.value) {
                        document.getElementById("ConfirmNewPassword").setCustomValidity("");
                        document.getElementById("CurrentPassword").setCustomValidity("");
                        document.getElementById("CurrentPasswordResult").style.visibility = "hidden";

                        jQuery.ajax({
                            url: '../Scripts/php/ChangePassword.php',
                            method: "POST",
                            data: {Name: Name, NewPassword: NewPasswordValue, Access: true},
                            success: function () {
                                window.location.href = "Account.php";
                            }
                        });
                    }else{
                        document.getElementById("ConfirmNewPassword").setCustomValidity("Passwords do not match");
                        document.getElementById("ConfirmNewPassword").reportValidity()
                    }
                }
            }
        });
}