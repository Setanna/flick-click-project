/* Login */
if(document.getElementById("LoginForm")){
    //Setup Variables
    let LoginForm = document.getElementById("LoginForm");
    let Email = document.getElementById("LoginEmail");
    let Password = document.getElementById("LoginPassword");

// Prevent Default
    $("#LoginForm").submit(function (Form) {
        Form.preventDefault();
    });


//Unobtrusive Javascript
    LoginForm.onsubmit = function () {
        if (Email.value !== "" && Password.value !== "") {
            LoginCheck(Email.value, Password.value)
        } else {
            alert("Please fill out the login form.")
        }
    };
}