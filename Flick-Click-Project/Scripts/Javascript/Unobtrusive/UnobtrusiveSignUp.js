// Setup Variables
let Username = document.getElementById("SignUpUsername");
let SignUpEmail = document.getElementById("Email");
let ConfirmPassword = document.getElementById("ConfirmPassword");

// Unobtrusive Javascript
Username.oninput = function() {CheckNameAvailability(Username.value)};
SignUpEmail.oninput = function() {CheckEmailAvailability(SignUpEmail.value)};
ConfirmPassword.oninput = function() {CheckPasswordMatch(ConfirmPassword)};