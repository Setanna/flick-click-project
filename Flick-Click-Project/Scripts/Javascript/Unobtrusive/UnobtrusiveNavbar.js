// variables
let Modal = document.getElementById("CookieModal");
let Accept = document.getElementById("Accept");

// Onload open the modal
document.addEventListener("DOMContentLoaded", function() {
    OpenModal();
});

// if user clicks the EssentialCookies button and it exists. close modal.
if(Accept){
    Accept.onclick = function() {
        jQuery.ajax({
            url: '../Scripts/php/AcceptCookies.php',
            method: "POST",
            success: function () {
            }
        });
        CloseModal();
    }
}

// Function
function OpenModal() {
    if(Modal){
        Modal.style.display = "block";
    }
}
function CloseModal() {
    if(Modal){
        Modal.style.display = "none";
    }
}
