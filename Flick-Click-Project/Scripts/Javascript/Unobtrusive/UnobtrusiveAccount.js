/* Account */
// Setup variables
let ChangePasswordRedirectButton = document.getElementById("ChangePasswordRedirectButton");
let ChangeEmailRedirectButton = document.getElementById("ChangeEmailRedirectButton");
let ClearCookies = document.getElementById("ClearCookies")

// Unobtrusive Javascript
$("#Form").submit(function(Form) {
    Form.preventDefault();
});
ChangePasswordRedirectButton.onclick = function() {location.href = "ChangePassword.php";};
ChangeEmailRedirectButton.onclick = function() {location.href = "ChangeEmail.php";};
ClearCookies.onclick = function() {
    jQuery.ajax({
        url: '../Scripts/php/DeleteCookies.php',
        method: "POST",
        success: function () {
            location.reload();
        }
    });
};
