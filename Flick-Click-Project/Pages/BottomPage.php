<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../StyleSheets/CombinedCssFiles.css" />
    <meta charset="utf-8" />
    <title>Flick Click</title>
</head>
<body>
    <div class="BottomPage">
        <div class="News">
            <a class="BlackTitleText">NEWS</a>
            <a class="ShowAllText" href="">Show all</a>
            <div class="DisplayBox">
                <div class="NewsBox">
                    <a class="DateText">09.02.2021</a>
                    <a class="BottomNewsTitle">Title</a>
                    <a class="ResumeText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus, diam vitae consectetur dapibus, turpis elit euismod erat, nec bibendum lacus nulla sed lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</a>
                    <a class="ReadMore" href="">Read More</a>
                </div>
                <div class="NewsBox">
                    <a class="DateText">09.02.2021</a>
                    <a class="BottomNewsTitle">Title</a>
                    <a class="ResumeText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus, diam vitae consectetur dapibus, turpis elit euismod erat, nec bibendum lacus nulla sed lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</a>
                    <a class="ReadMore" href="">Read More</a>
                </div>
                <div class="NewsBox">
                    <a class="DateText">09.02.2021</a>
                    <a class="BottomNewsTitle">Title</a>
                    <a class="ResumeText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus, diam vitae consectetur dapibus, turpis elit euismod erat, nec bibendum lacus nulla sed lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</a>
                    <a class="ReadMore" href="">Read More</a>
                </div>
            </div>
        </div>
        <div class="ComingSoon">
            <a class="BlackTitleText">COMING SOON</a>
            <a class="ShowAllText" href="">Show all</a>
            <div class="DisplayBox">
                <div class="ComingSoonBox">
                    <a class="DateText">09.02.2021</a>
                    <a class="BottomNewsTitle">Movie Title</a>
                    <div class="ComingSoonPictureText">
                        <img src="../FlickClickImages/ShrekPoster.jpeg"  />
                        <div>
                            <a class="ResumeText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus, diam vitae consectetur dapibus, turpis elit euismod erat, nec bibendum lacus nulla sed lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</a>
                            <a class="ReadMore" href="">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="ComingSoonBox">
                    <a class="DateText">09.02.2021</a>
                    <a class="BottomNewsTitle">Title</a>
                    <div class="ComingSoonPictureText">
                        <img src="../FlickClickImages/Shrek2Poster.jpg" />
                        <div>
                            <a class="ResumeText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus, diam vitae consectetur dapibus, turpis elit euismod erat, nec bibendum lacus nulla sed lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</a>
                            <a class="ReadMore" href="">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</body>
</html>