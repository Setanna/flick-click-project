﻿<?php
include 'NavBar.php';
?>

<!-- Keep from accessing by url -->
<?php
if(!isset($_SESSION["ID"])){
    header('Location: Home.php');
}
?>

<!-- Get User Data -->
<?php
define('AccessGranted', TRUE);
require '../Scripts/php/DatabaseLogin.php';
$query = mysqli_query($conn, "SELECT * FROM `User` WHERE Username = '".$_SESSION["Name"]."'");
$result = mysqli_fetch_array($query);
?>

<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div class="HomeContent"> <!-- TODO: Add Css -->
    <!-- Account -->
    <div class="Account">
        <table>
            <!-- Name -->
            <tr>
                <th>
                    <a>Name: </a>
                </th>
                <th>
                    <?php
                    echo "<a>".$result["Username"]."</a>";
                    ?>
                </th>
                <th>
                </th>
            </tr>
            <!-- Email -->
            <tr>
                <th>
                    <a>Email: </a>
                </th>
                <th>
                    <?php
                    echo "<a>".$result["Email"]."</a>";
                    ?>
                </th>
                <th>
                    <button id="ChangeEmailRedirectButton">Edit</button>
                </th>
            </tr>
            <!-- Password -->
            <tr>
                <th>
                    <a>Password: </a>
                </th>
                <th>
                    <a>•••••••••••••••••••••••••••••••••</a>
                </th>
                <th>
                    <button id="ChangePasswordRedirectButton">Edit</button>
                </th>
            </tr>
            <!-- Clear Cookies -->
            <tr>
                <th>
                    <a>Clear Cookies</a>
                </th>
                <th>

                </th>
                <th>
                    <Button id="ClearCookies" class="ClearCookiesButton">Clear</Button>
                </th>
                <th> <!-- TODO: Add Info Icon -->
                    <img src="../Resources/InfoIcon.png" alt="" title="This will log you out.">
                </th>
            </tr>
        </table>
    </div>
</div>
<!-- Scripts -->
<script src="../Scripts/Javascript/Unobtrusive/UnobtrusiveAccount.js"></script>
</body>
</html>