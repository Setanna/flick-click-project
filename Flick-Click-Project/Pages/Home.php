﻿<?php
include 'NavBar.php';
?>

<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <div class="HomeContent">
        <div class="LatestTrailers">
            <a class="RedTitleText">LATEST TRAILERS</a>
            <a class="ShowAllText" href="">Show all</a>
            <div class="DisplayMovieRow">
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/ShrekPoster.jpeg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek2Poster.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek3Poster.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek4Poster.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/ShrekAlternatePoster.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/ShrekAlternatePoster2.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="MostCommented">
            <a class="RedTitleText">MOST COMMENTED</a>
            <a class="ShowAllText" href="">Show all</a>
            <div class="DisplayMovieRow">
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek2AlternatePoster.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek2AlternatePoster2.png" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek3AlternatePoster.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek3AlternatePoster2.jpeg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek4AlternatePoster.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
                <div class="MovieDisplay">
                    <img class="MoviePoster" src="../FlickClickImages/Shrek4AlternatePoster2.jpg" />
                    <div class="TextBubble">
                        <a class="TextBubbleText">2</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include 'BottomPage.php';
        ?>
    </div>
</body>
</html>