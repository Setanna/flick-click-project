﻿<?php
include 'NavBar.php';
?>

<?php
session_start();
?>

<!-- Keep from accessing by url -->
<?php
if($_SESSION["ADMIN"] === "0"){
    header('Location: Home.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<body>
<div class="HomeContent"> <!-- TODO: Add Css -->
    <form action="../Scripts/php/AddMovie.php" id="UploadMovieImage" method="post" enctype="multipart/form-data">
        Movie Title:
        <input type="text" placeholder="Movie title" name="MovieTitle" id="MovieTitle" required>
        <br>
        select image to upload:
        <input type="file" name="MovieImage" id="MovieImage" required>
        <br>
        <input type="submit" value="Upload Image" name="submit">
    </form>
</div>
</body>
</html>
