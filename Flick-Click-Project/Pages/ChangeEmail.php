<!-- Includes -->
<?php
include 'NavBar.php';
?>

<!-- Keep from accessing by url -->
<?php
if(!isset($_SESSION["ID"])){
    header('Location: Home.php');
}
?>

<html lang="en">
<head>
    <title></title>
</head>

<body>
<div class="HomeContent"> <!-- TODO: Add Css -->
    <!-- Change Email -->
    <form class="ChangeAccount" id="Form" method="post">
        <table>
            <!-- Password -->
            <tr>
                <th>
                    <a>Password</a>
                </th>
                <th>
                    <label><input id="Password" name="Password" placeholder="Password" type="password" maxlength="50" required /></label>
                </th>
                <th class="ErrorResult">
                    <img id="PasswordResult" style="visibility: hidden" alt="" src=""/>
                </th>
            </tr>
            <!-- Email -->
            <tr>
                <th>
                    <a>New Email</a>
                </th>
                <th>
                    <label><input id="Email" name="Email" placeholder="New Email" type="email" maxlength="250" required/></label>
                </th>
                <th class="ErrorResult">
                    <img id="EmailAvailability" style="visibility: hidden" alt="" src=""/>
                </th>
            </tr>
        </table>
        <input id="ChangeEmailButton" class="ButtonEmail" type="submit" value="Change Email"/>
    </form>
</div>
<!-- Scripts -->
<script src="../Scripts/Javascript/AccountEdits.js"></script>
<script>
    // TODO: Make work in UnobtrusiveChangeEmail.js
    /* Change Email */
    $("#Form").submit(function(Form) {
        Form.preventDefault();
    });

    // Setup Variables
    let Email = document.getElementById("Email");
    let ChangeEmailButton = document.getElementById("ChangeEmailButton");
    let Password = document.getElementById("Password");

    // Unobtrusive Javascript
    Email.oninput = function() {CheckEmailAvailability(Email.value)};
    ChangeEmailButton.onclick = function() {ChangeEmail("<?php echo $_SESSION['Name'] ?>", Email.value, Password.value)};
</script>
</body>
</html>
