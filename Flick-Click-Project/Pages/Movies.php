﻿<?php
include 'NavBar.php';
?>

<?php
session_start();
?>

<!-- Keep from accessing by url -->
<?php
if($_SESSION["ADMIN"] === "0"){
    header('Location: Home.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<body>
<div class="HomeContent"> <!-- TODO: Add Css -->
    <a href="AddMovie.php">Add Movie</a>
    <a href="EditMovie.php">Edit Movie</a>
    <a href="DeleteMovie.php">Delete Movie</a>
</div>
</body>
</html>
