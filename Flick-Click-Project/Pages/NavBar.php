﻿<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../StyleSheets/CombinedCssFiles.css" />
    <meta charset="utf-8" />
    <title>Flick Click</title>
</head>
<body>
    <header>
        <div class="NavBar">
            <div class="PageBar">
                <img class="FlickClickLogo" src="../FlickClickImages/Logo.png"  alt=""/>
                <div class="Pages">
                    <a id="Home" class="Text" href="Home.php">HOME</a>
                    <a id="News" class="Text" href="News.php">NEWS</a>
                    <a id="Contact" class="Text" href="Contact.php">CONTACT</a>
                </div>
            </div>
            <div class="MovieBar">
                <div class="CenterVerticalLeft">
                    <a class="Text">SHOW ALL</a>
                    <a class="Text">LATEST TRAILERS</a>
                    <a class="Text">MOST COMMENTED</a>
                </div>
                <form class="CenterVerticalRight">
                    <a class="Text">SEARCH</a>
                    <label>
                        <input placeholder="Enter search here" />
                    </label>
                    <submit class="Text">GO!</submit>
                </form>
            </div>
            <div class="MemberBar">
                <?php
                if(!isset($_SESSION["ID"])) {
                    echo "
                        <a class='Text CenterVerticalRight' href='SignUp.php'>Not a Member?</a>
                        <form id='LoginForm' method='post' class='CenterVerticalRight'>
                            <input id='LoginEmail' type='email' placeholder='Email' />
                            <input id='LoginPassword' type='password' placeholder='Password' />
                            <!-- TODO FIX: css navbar sign in button -->
                            <b><input type='submit' id='Submit' class='SignIn' value='SIGN IN' /></b>
                        </form>
                    ";
                }elseif($_SESSION["Admin"] === "1"){ // TODO FIX: This shows up for non admin users.
                    echo "
                        <a class='Text CenterVerticalRight' href='../Pages/Movies.php'>Movies</a>
                        <a class='Text CenterVerticalRight' href='../Pages/Account.php'>Account</a>
                        <a class='Text CenterVerticalRight' href='../Scripts/php/Logout.php'>Logout</a>
                    ";
                }else{
                    echo "
                        <a class='Text CenterVerticalRight' href='../Pages/Account.php'>Account</a>
                        <a class='Text CenterVerticalRight' href='../Scripts/php/Logout.php'>Logout</a>
                    ";
                }
                ?>
            </div>
        </div>
    </header>
    <div class="BotBar">
        <div class="Pages">
            <div class="TextCenterVerticalMiddle BotBarText">
                <a class="BotBarText" href="Home.php">HOME</a> | <a class="BotBarText" href="News.php">NEWS</a> | <a class="BotBarText" href="Contact.php">CONTACT</a>
            </div>
        </div>
        <div class="SocialMedias">
            <div class="TextCenterVerticalMiddle">
                <!-- TODO: Add Social Media Logos -->
            </div>
        </div>
        <div class="TimeStamp">
            <div class="TextCenterVerticalMiddle BotBarText">
                © 2020 Flick Click ApS
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="../Scripts/Javascript/Unobtrusive/UnobtrusiveLogin.js"></script>
    <script src="../Scripts/Javascript/Unobtrusive/NavBar.js"></script>
    <script src="../Scripts/Javascript/AccountChecks.js"></script>
</body>
</html>
