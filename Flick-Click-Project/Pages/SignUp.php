﻿<?php
include 'NavBar.php';
?>

<!-- Keep from accessing by url -->
<?php
if(isset($_SESSION["ID"])){
    header('Location: Home.php');
}
?>

<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <div class="SignUpContent">
        <form action="../Scripts/php/SignUpSend.php" method="post">
            <table>
                <tr>
                    <th>
                        <b>Username:</b>
                    </th>
                    <th>
                        <label for="SignUpUsername"></label><input id="SignUpUsername" Name="SignUpUsername" type="text" placeholder="Username" maxlength="250" required/>
                    </th>
                </tr>
                <tr>
                <tr>
                    <th>
                        <b>Email:</b>
                    </th>
                    <th>
                        <label for="Email"></label><input id="Email" name="Email" type="email" placeholder="Email" maxlength="250" required/>
                    </th>
                </tr>
                <tr>
                    <th>
                        <b>Password:</b>
                    </th>
                    <th>
                        <label for="CheckPassword"></label><input id="CheckPassword" Name="CheckPassword" type="password" placeholder="Password" maxlength="250" required/>
                    </th>
                </tr>
                <tr>
                    <th>
                        <b>Confirm Password:</b>
                    </th>
                    <th>
                        <label for="ConfirmPassword"></label><input id="ConfirmPassword" type="password" placeholder="Confirm Password" required/>
                    </th>
                </tr>
                <tr>
                    <th>
                        <input name="submit" type="submit" value="Sign Up" />
                    </th>
                </tr>
            </table>
        </form>
    </div>
    <script src="../Scripts/Javascript/Unobtrusive/UnobtrusiveSignUp.js"></script>
</body>
</html>