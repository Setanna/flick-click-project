<!-- Includes -->
<?php
include 'NavBar.php';
?>

<!-- Keep from accessing by url -->
<?php
if(!isset($_SESSION["ID"])){
    header('Location: Home.php');
}
?>

<html lang="en">
<head>
    <title></title>
</head>
<body>
<div class="HomeContent"> <!-- TODO: Add Css -->
    <!-- Change Password -->
    <form class="ChangeAccount" id="Form" method="post">
        <table>
            <!-- Current Password -->
            <tr>
                <th>
                    <a>Current Password</a>
                </th>
                <th>
                    <label><input id="CurrentPassword" name="CurrentPassword" placeholder="Current Password" type="password" maxlength="50" required /></label>
                </th>
                <th class="ErrorResult">
                    <img id="CurrentPasswordResult" style="visibility: hidden" alt="" src=""/>
                </th>
            </tr>
            <!-- New Password -->
            <tr>
                <th>
                    <a>New Password</a>
                </th>
                <th>
                    <label><input id="NewPassword" name="NewPassword" placeholder="New Password" type="password" maxlength="50" required/></label>
                </th>
            </tr>
            <!-- Confirm New Password -->
            <tr>
                <th>
                    <a>Confirm New Password</a>
                </th>
                <th>
                    <label><input id="ConfirmNewPassword" name="ConfirmNewPassword" placeholder="Confirm New Password" type="password" maxlength="50" required oninput="CheckPasswordMatch(this)"/></label>
                </th>
            </tr>
        </table>
        <input id="ChangePasswordButton" class="ButtonPassword" type="submit" value="Change Password"/>
    </form>
</div>
<!-- Scripts -->
<script src="../Scripts/Javascript/AccountEdits.js"></script>
<script src="../Scripts/Javascript/Unobtrusive/UnobtrusiveChangePassword.js"></script>
<script>
    // TODO: Make work in UnobtrusiveChangePassword.js
    /* Change Password */
    // Setup Variables
    let ChangePasswordButton = document.getElementById("ChangePasswordButton");
    let CurrentPassword = document.getElementById("CurrentPassword");
    let NewPassword = document.getElementById("NewPassword");
    let ConfirmNewPassword = document.getElementById("ConfirmNewPassword");


    // Unobtrusive Javascript
    $("#Form").submit(function(Form) {
        Form.preventDefault();
    });
    ChangePasswordButton.onclick = function() {EditPassword('<?php echo $_SESSION["Name"] ?>', CurrentPassword.value, NewPassword.value)};
</script>
</body>
</html>
